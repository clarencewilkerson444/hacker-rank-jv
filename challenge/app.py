# initial draft that we did
def palindrome(input_string: str) -> bool:
    left_to_right = input_string.split(sep=None)
    right_to_left = input_string.split(sep=None)[::-1]
    return left_to_right == right_to_left


"""is_palindrome will determine if the provided word is a paldinrome or not
@word string
output bool
"""
def is_palindrome(word: str) -> bool:
    word_length = len(word) - 1
    is_a_palindrome = True
    i = 0
    middle_of_the_word = int(len(word) / 2)

    for character in word:
        if middle_of_the_word == i:
            break

        if character != word[word_length]:
            is_a_palindrome = False
            break

        i = i + 1
        word_length = word_length - 1

    return is_a_palindrome
