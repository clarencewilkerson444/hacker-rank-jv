from typing import List


def greatestNumber(array):
    """
    The following function finds the greatest single number within an array,
    but has an efficiency of O(N^2).
    Rewrite the function so that it becomes a speedy O(N):
    """
    for i in array:
        # Assume for now that i is the greatest:
        isIValTheGreatest = True
        for j in array:
            # If we find another value that is greater than i,
            # i is not the greatest:
            if j > i:
                isIValTheGreatest = False

            # If, by the time we checked all the other numbers, i
            # is still the greatest, it means that i is the greatest number:
            if isIValTheGreatest:
                return i


def greatest_number_improved(array: List) -> int:
    max_number = float("-inf")

    for position, element in enumerate(array):
        if element > max_number:
            max_number = element

    return max_number
