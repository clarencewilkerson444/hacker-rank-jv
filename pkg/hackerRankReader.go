package pkg

import (
	"bufio"
	"io"
	"os"
	"strconv"
	"strings"
)

// LineExtractor will
func LineExtractor(peopleInLine int, lineOfPeople []string) []int32 {
	var peopleLine []int32
	for i := 0; i < peopleInLine; i++ {
		qItemTemp, err := strconv.ParseInt(lineOfPeople[i], 10, 64)
		checkError(err)
		person := int32(qItemTemp)
		peopleLine = append(peopleLine, person)
	}
	return peopleLine
}

// ObtainReader will obtain the reader
func ObtainReader(testCasePath string) io.Reader {
	fixture, error := os.Open(testCasePath)
	defer fixture.Close()
	checkError(error)
	return bufio.NewReaderSize(fixture, 1024*1024)
	//firstMultipleInput := strings.Split(strings.TrimSpace(readLine(reader)), " ")
	//log.Printf("%v", firstMultipleInput)
	//checkError(err)
	//fixtureLines := int32(tTemp)
	//TestCases := make(map[int][]int32)

	//for tItr := 0; tItr < int(fixtureLines); tItr++ {
	//	literalPeopleInLine, err := strconv.ParseInt(readLine(reader), 10, 64)
	//	checkError(err)
	//	lineOfPeople := strings.Split(readLine(reader), " ")
	//	peopleInLine = int(literalPeopleInLine)
	//	TestCases[tItr] = LineExtractor(peopleInLine, lineOfPeople)
	//}
	//return TestCases
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func ReadLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}
