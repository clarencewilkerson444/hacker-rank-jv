import unittest

from main import compressor


class TestCompressor(unittest.TestCase):
    def test_compressor_base_case(self):
        sample = "abcaaabbb"
        output = compressor(sample)
        self.assertEqual("abca3b3", output)


if __name__ == '__main__':
    unittest.main()
