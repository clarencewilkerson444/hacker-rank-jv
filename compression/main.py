from contextlib import closing
from io import StringIO


def compressor(message: str) -> str:
    """
    Compressor will compress a string and produce
    the count of repeated elements.

    :param message:
    :return:
    """

    max_chars_to_read = len(message)
    compressed_output = StringIO()
    j = 0
    last_read_character = ""

    with closing(compressed_output) as temporary_string_counter:
        for index in range(0, max_chars_to_read):
            current_character = message[index]

            if j > 0 and last_read_character != current_character:
                next_words = message[index + j + 1:]
            else:
                next_words = message[index + 1:]
                j = 0

            next_words_size = len(next_words)
            # Reset index
            accumulator = 1

            while (j < next_words_size and
                   current_character == next_words[j] and
                   last_read_character != current_character):
                j = j + 1
                accumulator = accumulator + 1

            if accumulator > 1 and last_read_character != current_character:
                temporary_string_counter.write(f"{current_character}{accumulator}")
            elif last_read_character != current_character:
                temporary_string_counter.write(f"{current_character}")

            last_read_character = current_character

        return compressed_output.getvalue()
